python3 -m venv venv
source venv/bin/activate
poetry install --with test
poetry run pytest tests/unit
poetry run pytest tests/integration
