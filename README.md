# Vault Data Retrieval Tool
This tool is designed to retrieve secrets and secret metadata from HashiCorp Vault using the AppRole authentication method. It provides functions to fetch passwords and key data from a specified path within the Vault.

## TODO
Add poetry install instructions

## Requirements
* Python 3.x
* pip (Python package manager)

## Installation
1. Clone the repository from https://code.europa.eu/digit-c4/dev/ansible-vault.
2. Navigate to the project directory.
3. Create a virtual environment (optional but recommended):
    ```sh
    python3 -m venv venv
    source venv/bin/activate
    ```
4. Install dependencies using the provided requirements.txt file:
    ```sh
    pip install -r requirements.txt
    ```

## Usage
1. Ensure you have the necessary permissions and access to the HashiCorp Vault.
2. Set up the AppRole authentication method in your Vault instance. Refer to the provided links for detailed instructions.
3. Modify the code to provide your Vault URL, namespace, role ID, secret ID, mount point, and engine details.
4. Example:
    ```python
    VAULT_KEY = getenv("VAULT_KEY")
    VAULT_MOUNT_POINT = getenv("VAULT_MOUNT_POINT")

    with Vault(url="http://vault.example.com", namespace="GROUP/NAMESPACE", role_id="00000000-0000-0000-0000-000000000000", secret_id="00000000-0000-0000-0000-000000000000") as vault:
        secret = vault.get_keys_data(VAULT_KEY, VAULT_MOUNTPOINT)
    ```

## Functionality
* `get_password_by_app_role(key, mount_point)`: This function retrieves a password from the specified key path in the Vault.
* `get_keys_data(key, mount_point)`: This function retrieves key data (secret metadata) from the specified key path in the Vault.

## Notes
* Ensure that the AppRole authentication method is correctly configured and enabled in your Vault instance.
* Modify the code according to your specific Vault configuration and requirements.
* Handle errors and exceptions appropriately in your code to ensure smooth operation.

## References
* HashiCorp Vault Documentation: [https://www.vaultproject.io/docs](https://www.vaultproject.io/docs)
* HVAC Documentation: [https://hvac.readthedocs.io](https://hvac.readthedocs.io)

## Other useful links
* https://developer.hashicorp.com/vault/docs/auth/approle
* https://hvac.readthedocs.io/en/stable/usage/auth_methods/approle.html
* https://hvac.readthedocs.io/en/stable/usage/secrets_engines/kv_v2.html#read-secret-metadata

## Authors
* Marcelo Teixeira
* Ricardo Silva
