"""
Hashicorp Vault Python class and helpers

TODO LIST:
- Identify what object types the methods returns
- Make sure every method argument is of the correct type
- Use context manager to avoid having to login...logout in each method
"""
from logging import Logger, DEBUG as LOG_DEBUG, INFO as LOG_INFO, WARNING as LOG_WARNING, ERROR as LOG_ERROR, CRITICAL as LOG_CRITICAL
from enum import IntEnum
from typing import Union
from requests import Session as RequestSession
from hvac import Client as HVACClient


class LogLevel(IntEnum):
    """Log level"""
    DEBUG = LOG_DEBUG
    INFO = LOG_INFO
    WARNING = LOG_WARNING
    ERROR = LOG_ERROR
    CRITICAL = LOG_CRITICAL

    @classmethod
    def _missing_(cls, value: str):
        for member in cls:
            if member.name.lower() == value.lower():
                return member
        return None


class Vault:
    """
    Hashicorp Vault wrapper class.

    Basic usage:
    with Vault(url="http://vault.example.com", namespace="GROUP/NAMESPACE", role_id="00000000-0000-0000-0000-000000000000", secret_id="00000000-0000-0000-0000-000000000000") as vault:
        secret = vault.get_keys_data(VAULT_KEY, VAULT_MOUNTPOINT)
    """

    def __init__(self, url: str, namespace: str, role_id: str, secret_id: str, verify: bool = False, logger: Union[Logger, None]=None):
        self.session = RequestSession()
        self.hvac_client = HVACClient(
            url=url, namespace=namespace,  verify=verify)
        self.role_id = role_id
        self.secret_id = secret_id
        self.logger = logger

    def __enter__(self):
        """Login the HVAC client when entering the runtime context related to this object"""
        self.__login()
        return self

    def __exit__(self, *args):
        """Logout the HVAC client when exiting the runtime context of the object"""
        self.__logout()

    def __login(self) -> None:
        """Login the HVAC client"""
        response = self.hvac_client.auth.approle.login(
            role_id=self.role_id, secret_id=self.secret_id)
        # Extract the client token from the response
        self.hvac_client.token = response['auth']['client_token']

    def __logout(self) -> None:
        """Logout the HVAC client"""
        self.hvac_client.logout()

    def __log(self, message: str, level: str = "debug") -> None:
        """Logs or print messages"""
        if isinstance(self.logger, Logger):
            self.logger.log(LogLevel(level).value, message)
            return
        print(f"[{level.upper()}] {message}")

    def set_logger(self, logger: Logger) -> None:
        """Add logging capabilities to this object"""
        self.logger = logger

    def read_secret(self, key: str, mount_point: str) -> Union[dict, None]:
        """Reads a secret"""
        self.__log(f"Acquiring secret {key} on mount point {mount_point}...", "debug")
        secret = self.hvac_client.secrets.kv.v2.read_secret_version(
            path=key, mount_point=mount_point)
        if secret is not None:
            self.__log("Success", "debug")
            return secret
        self.__log(f"Failed to retrieve secret {key} on mount point {mount_point}", "error")
        return None

    def get_password_by_app_role(self, key: str, mount_point: str) -> str:
        """Gets a password"""
        # TODO: Acc using version 1 in the past now all is uniform
        secret = self.hvac_client.secrets.kv.v2.read_secret_version(
            path=key, mount_point=mount_point)
        secret_data = False
        if secret is not None and 'data' in secret and secret['data'] is not None:
            secret_data = secret['data']['data']['data']['password']
        else:
            self.__log(f"{secret} secret")
            self.__log("Failed to retrieve the secret.")
        return secret_data

    def get_keys_data(self, key: str, mount_point: str) -> dict:
        """Gets keys data"""
        # TODO : Acc using version 1 in the past now all is uniform
        secret = self.hvac_client.secrets.kv.v2.read_secret_version(
            path=key, mount_point=mount_point)
        secret_data = False
        if secret is not None and 'data' in secret and secret['data'] is not None:
            secret_data = secret['data']['data']
        else:
            self.__log(f"{secret} secret")
            self.__log("Failed to retrieve the secret.")
        return secret_data
