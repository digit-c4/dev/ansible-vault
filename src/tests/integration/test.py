"""
Test vault connector
"""
# pylint: disable=wrong-import-position, import-error
from sys import path as sys_path
from os import getenv
from logging import DEBUG
from unittest import TestCase, main as unittest_main
sys_path.append("../../vault")
from client import Vault
from utils import create_logger

# Constants
VAULT_KEY = getenv("VAULT_KEY")
VAULT_NAMESPACE = getenv("VAULT_NAMESPACE")
VAULT_URL = getenv("VAULT_URL")
VAULT_APPROLE_ID = getenv("VAULT_APPROLE_ID")
VAULT_APPROLE_PWD = getenv("VAULT_APPROLE_PWD")
VAULT_MOUNT_POINT = getenv("VAULT_MOUNT_POINT")
ALT_VAULT_NAMESPACE = getenv("ALT_VAULT_NAMESPACE")
ALT_VAULT_KEY = getenv("ALT_VAULT_KEY")
ALT_VAULT_APPROLE_ID = getenv("ALT_VAULT_APPROLE_ID")
ALT_VAULT_APPROLE_PWD = getenv("ALT_VAULT_APPROLE_PWD")
SECRET_VALUE = getenv("SECRET_VALUE")
SECRET_KEY = getenv("SECRET_KEY")
SECRET_CREATED_TIME = getenv("SECRET_CREATED_TIME")

# Globals
logger = create_logger(__name__, None, level=DEBUG)


class Test(TestCase):
    """Vault test"""
    def test_read_secret(self):
        """Reads a secret"""
        with Vault(url=VAULT_URL, namespace=VAULT_NAMESPACE, role_id=VAULT_APPROLE_ID, secret_id=VAULT_APPROLE_PWD, logger=logger) as vault:
            secret = vault.read_secret(VAULT_KEY, VAULT_MOUNT_POINT)
            created_time = secret.get("data", {}).get("metadata", {}).get("created_time", "GET_CREATED_TIME_ERROR")
            self.assertEqual(SECRET_CREATED_TIME, created_time)

    def test_get_keys_data(self):
        """Gets keys data"""
        with Vault(url=VAULT_URL, namespace=VAULT_NAMESPACE, role_id=VAULT_APPROLE_ID, secret_id=VAULT_APPROLE_PWD, logger=logger) as vault:
            aws_secret = vault.get_keys_data(VAULT_KEY, VAULT_MOUNT_POINT)
            self.assertEqual(SECRET_VALUE, aws_secret.get("data", {}).get(SECRET_KEY, "SECRET_KEY_GET_ERROR"))

    def test_get_password(self):
        """Gets a password"""
        with Vault(url=VAULT_URL, namespace=ALT_VAULT_NAMESPACE, role_id=ALT_VAULT_APPROLE_ID, secret_id=ALT_VAULT_APPROLE_PWD, logger=logger) as vault:
            password = vault.get_password_by_app_role(ALT_VAULT_KEY, VAULT_MOUNT_POINT)
            self.assertEqual("toto", password)


if __name__ == '__main__':
    unittest_main()
