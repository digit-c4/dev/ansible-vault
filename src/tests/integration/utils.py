"""
Miscellaneous utils
"""
from enum import Enum
from logging import INFO, StreamHandler, Formatter, Logger, getLogger
from logging.handlers import RotatingFileHandler


class LoggingType(Enum):
    """
    Enum for logging types.
    """
    STREAM = 0
    FILE = 1
    BOTH = 2


def create_logger(
    name: str,
    path: str,
    output_type: LoggingType = LoggingType.STREAM,
    level: int = INFO,
    max_size: int = 50 * 1024,
    backup_count: int = 0,
) -> Logger:
    """
    Initialize a new logger.

    Args:
        (str) name:         the name of the logger (usually the module name -> ie. __name__)
        (str) path:         the path to the log file (can be None if type is 'stream')
        (str) output_type:  the type of logging handler to use (stream -> ie. console, file or both)
        (int) level:        the logging level (default is logging.DEBUG)
        (int) max_size:     the maximum size of the log file in bytes (default is 50*1024)
        (int) backup_count: the number of log files to keep (default is 0)

    Returns:
        (logging.Logger) the initialized logger
    """
    # pylint: disable=too-many-arguments
    formatter = Formatter(
        "[{asctime}] {name:<12s} {funcName:<28} L{lineno:<03} {levelname:<8s} {message}",
        style="{",
    )
    logger = getLogger(name)
    logger.setLevel(level)

    if output_type == LoggingType.STREAM:
        stream_handler = StreamHandler()
        stream_handler.setFormatter(formatter)
        logger.addHandler(stream_handler)
    elif output_type == LoggingType.FILE:
        file_hander = RotatingFileHandler(
            path, maxBytes=max_size, backupCount=backup_count
        )
        file_hander.setFormatter(formatter)
        logger.addHandler(file_hander)
    elif output_type == LoggingType.BOTH:
        stream_handler = StreamHandler()
        stream_handler.setFormatter(formatter)
        file_hander = RotatingFileHandler(
            path, maxBytes=max_size, backupCount=backup_count
        )
        file_hander.setFormatter(formatter)
        logger.addHandler(stream_handler)
        logger.addHandler(file_hander)

    return logger
